/*
1. In the S15 folder, create an a1 folder, an index.html file inside of it and link the index.js file.

2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.

3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
- If the total of the two numbers is less than 10, add the numbers
- If the total of the two numbers is 10 - 20, subtract the numbers
- If the total of the two numbers is 21 - 30 multiply the numbers
- If the total of the two numbers is greater than or equal to 31, divide the numbers

4. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.

5. Prompt the user for their name and age and print out different alert messages based on the user input:
-  If the name OR age is blank/null, print the message are you a time traveler?
-  If the name AND age is not blank, print the message with the user’s name and age.

6. Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
- 18 or greater, print an alert message saying You are of legal age.
- 17 or less, print an alert message saying You are not allowed here.

7. Create a switch case statement that will check if the user's age input is within a certain set of expected input:
- 18 - print the message You are now allowed to party.
- 21 - print the message You are now part of the adult society.
- 65 - print the message We thank you for your contribution to society.
- Any other value - print the message Are you sure you're not an alien?

8. Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.

9. Create a gitlab project repository named a1 in S15.

10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.

11. Add the link in Boodle.
*/
console.log("Hello, world!")

/*
3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
- If the total of the two numbers is less than 10, add the numbers
- If the total of the two numbers is 10 - 20, subtract the numbers
- If the total of the two numbers is 21 - 30 multiply the numbers
- If the total of the two numbers is greater than or equal to 31, divide the numbers

4. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.
*/
let num1 = parseInt(prompt("Please enter 1 number:"));
let num2 = parseInt(prompt("Enter another number:"));

function determineNumTotal (num1, num2) {
	return parseInt(num1+num2);
}

let numValidation = determineNumTotal(num1,num2);
// console.log(determineNumTotal(num1,num2));

if (numValidation < 10) {
	console.warn ("The sum of both numbers is " + (num1 + num2));
}
else if (numValidation >= 10 && numValidation <= 20) {
	if (num1 < num2) {
		alert ("The difference of both numbers is " + (num2 - num1));
	}
	else {
		alert ("The difference of both numbers is " + (num1 - num2));
	}
}
else if (numValidation >= 21 && numValidation <= 30) {
	alert ("The product of both numbers is " + (num1 * num2));
}
else if (numValidation >= 31) {
	alert ("The quotient of both numbers is " + (num1/num2));
}
else {
	alert("Please enter 2 numbers.")
}

/*
5. Prompt the user for their name and age and print out different alert messages based on the user input:
-  If the name OR age is blank/null, print the message are you a time traveler?
-  If the name AND age is not blank, print the message with the user’s name and age.
*/
let uName = prompt("What is your name?");
let uAge = parseInt(prompt("How old are you?")) * 1;

if (uName == "" || uAge == 0) {
	alert("Are you a time traveler?");
}
else if (uName !== "" || uAge !== 0) {
	alert("Your name is " + uName + " and you are " + uAge + " years old");
}
/*
6. Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
- 18 or greater, print an alert message saying You are of legal age.
- 17 or less, print an alert message saying You are not allowed here.

*/
function isLegalAge (uAge) {
	if (uAge >= 18) {
		alert ("You are of legal age.");
	}
	else {
		alert ("You are not allowed here.");
	}
}

let uCheckAage = isLegalAge(uAge);

/*
7. Create a switch case statement that will check if the user's age input is within a certain set of expected input:
- 18 - print the message You are now allowed to party.
- 21 - print the message You are now part of the adult society.
- 65 - print the message We thank you for your contribution to society.
- Any other value - print the message Are you sure you're not an alien?
*/

switch (true) {
	case (uAge == 65):
		alert("We thank you for your contribution to society.");
		break;
	case (uAge == 21):
		alert("You are now part of the adult society.");
		break;
	case (uAge == 18):
		alert("You are now allowed to party.");
		break;
	default:
		alert("Are you sure you're not an alien?")				
}

/*
8. Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.
*/
function forceError(uAge) {
	try {
		// Attempt to execute a code
		aaaaaaaaaaalert(isLegalAge(uAge));
	}
	// error/err are commonly used variables by developers for storing errors
	catch (error) {
		console.log(typeof error);
		console.warn(error.message);
		// error.message is used to access the information relating to an error object
	}
	finally {
		// Continue on executing the code regardless of success or failure of code execution in the 'try' block.
		alert(isLegalAge(uAge));
	}
}

forceError(25);